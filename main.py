# -*- coding: utf-8 -*-
import sys, os
sys.path.insert(0,'scripts')
from flask import Flask, render_template, session, request, flash, redirect, url_for, make_response, session 
import requests,forms
from collections import OrderedDict
import simplejson as json


app = Flask(__name__)
app.config.from_object("config")
app.secret_key = 'the-fuckin-secret-key'

############################# Login section ################################


@app.route('/', methods = ['GET','POST'])
@app.route('/login', methods = ['GET','POST'])
def root():
    return render_template('welcome_screen.html')

############################# Login section ################################

@app.route('/dashboard', methods = ['GET','POST'])
def dashboard():
    return render_template('dashboard.html',
                            profile_photo = 'https://myblue.bluecrossma.com/sites/g/files/csphws636/files/inline-images/Doctor%20Image%20Desktop.png',
                            ingresos_monto = '$35,000',
                            ingresos_mes = 'Septiembre',
                            proximo_pago = 'Octubre 23',
                            doctor_name = 'Dr. Ernesto Alfonso',
                            doctor_spec = 'Neurocirugia',
                            doctor_certificate = '0123456',
                            doctor_school = 'Instituto Politecnico Nacional')

@app.route('/google_calendar',methods = ['GET','POST'])
def google_calendar():
    return render_template('google_calendar.html')

if __name__ == '__main__':
    app.debug = True
    app.run()
