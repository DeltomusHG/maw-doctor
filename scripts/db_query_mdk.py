# -*- coding: UTF-8 -*-
import psycopg2, psycopg2.extras, xlwt, xlrd, json, hash_generator
from collections import OrderedDict
from datetime import datetime


def search_user_by_email(customer_email):
    response = [] #Una lista para almacenar diccionarios con todas las coincidencias
     #El diccionario para almacenar los id y tipo de usuario de cada resultado en la primera consulta a BD
    try:
        conn_be = psycopg2.connect(database = 'maw_prod', user = 'maw_user_prod', password = '4mK5Q5Cd8qXjGNLM78QCrHHAVALJDKExGZcnVEsTEZNWD', host = 'mawprod.ccw6u6yt41ar.us-east-1.rds.amazonaws.com')
        cur_be = conn_be.cursor()
        #Tratamos de conectar a la base de datos
    except:
        db_response = OrderedDict()
        db_response['error'] = 'Error: No se ha podido conectar con la base de datos.'
        response.append(db_response)
        return response
    
    try:
        cur_be.execute("""
        SELECT users.id,email,name,maiden_name,surname,telephone 
        FROM users INNER JOIN user_details ON (users.id = user_details.user_id)
        WHERE users.email LIKE '%{}%' 
        """.format(customer_email))
        if cur_be.rowcount != 0:
            for row in cur_be: #Este procedimiento es para un solo usuario, hay que planearlo para muchas usuarios de respuesta
                db_response = OrderedDict()
                db_response['id'] = int(row[0])
                db_response['email'] = row[1]
                db_response['name'] = row[2]
                db_response['maiden_name'] = row[3]
                db_response['surname'] = row[4]
                db_response['telephone'] = row[5]

                try:
                    db_response['type_of_user_id'] = int(row[6])
                except:
                    db_response['type_of_user_id'] = 0
                response.append(db_response)   
                #Se procesan todas las coincidencias y se guardan en la lista de resultados     
        else:
            db_response = OrderedDict()
            db_response['warning'] = 'Warning: No se encontró el registro solicitado.'
            response.append(db_response)
        cur_be.close()
        conn_be.close() #Cerramos el cursor y la conexión para no tenerlos abiertos
        #Se ejecuta la consulta
    except Exception as e: #Esto nos permite capturar el valor de la excepción para imprimirlo
        db_response = OrderedDict()
        db_response['error'] = 'Error: La búsqueda no se realizó correctamente.'
        response.append(db_response)
        print e
        return response

    return response

    





